module.exports = {
    get AWS () {
        return require('./AWS');
    },

    get S3 () {
        return require('./S3');
    }
};
